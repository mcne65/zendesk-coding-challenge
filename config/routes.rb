# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#home'
  get 'zendesk/list_tickets', to: 'zendesk#list_tickets', as: 'zendesk_list_tickets'
  get 'zendesk/show_ticket/:id', to: 'zendesk#show_ticket', as: 'zendesk_show_ticket'

  get '*path', to: redirect('/')
end
