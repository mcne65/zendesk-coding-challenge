# frozen_string_literal: true

module Errors
  class ZendeskError < StandardError
    attr_reader :status, :error, :message

    def initialize(error = nil, status = nil, message = nil)
      @error = error || :unprocessable_entity
      @status = status || 422
      @message = message || 'Something went wrong'
    end
  end
end
