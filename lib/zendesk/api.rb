# frozen_string_literal: true

require_relative '../http/get.rb'
require_relative '../errors/zendesk_error.rb'

module Zendesk
  class API
    attr_reader :code, :body

    def list_tickets(page, per_page)
      check_input(page) || check_input(per_page)
      get_data("#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=#{page}&per_page=#{per_page}")
      parse_data
      check_common_errors
      handle_empty_ticket_list(page, per_page, @body['count']) if @body['tickets'].empty?
    end

    def show_ticket(id)
      check_input(id)
      get_data("#{ENV['ZENDESK_URL']}/api/v2/tickets/#{id}.json")
      parse_data
      check_common_errors
    end

    private

    def parse_data
      @code = @response.code.to_i
      @body = @response.body.empty? ? nil : JSON.parse(@response.body)
    end

    def get_data(url)
      request = HTTP::Get.new(url)
      request.basic_auth(ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD'])
      @response = request.send
    end

    def check_common_errors
      raise Errors::ZendeskError.new('Zendesk', @code, 'Unauthorized access response') if @code.eql?(401)
      raise Errors::ZendeskError.new('Zendesk', @code, (@body['error']['title'] || @body['error'])) if @code.eql?(404)
      raise Errors::ZendeskError.new('Unexpected Zendesk Response', @code, @body) unless @code.eql?(200)
    end

    def check_input(parameter)
      message = 'Parameter given is not parsable as an integer'
      raise Errors::ZendeskError.new('Internal Error', 500, message) unless /^\d+$/ =~ parameter
    end

    def handle_empty_ticket_list(page, per_page, ticket_count)
      starting_ticket = (page.to_i * per_page.to_i) - per_page.to_i + 1
      message = "Ticket list requested starts from #{starting_ticket} but there are only #{ticket_count} tickets"
      raise Errors::ZendeskError.new('Zendesk', 204, message)
    end
  end
end
