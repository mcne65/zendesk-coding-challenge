# frozen_string_literal: true

require_relative '../errors/http_error.rb'

module HTTP
  class Get
    attr_reader :request, :uri

    def initialize(url)
      create_uri(url)
      @request = Net::HTTP::Get.new(@uri)
    end

    def send
      Net::HTTP.start(@uri.hostname, @uri.port, ssl_options) do |http|
        http.request(@request)
      end
    end

    def basic_auth(username, password)
      raise_error(username, 'not of type String') { |x| x.is_a? String }
      raise_error(password, 'not of type String') { |x| x.is_a? String }
      @request.basic_auth(username, password)
    end

    def request_headers(headers)
      raise_error(headers, 'not of type Hash') { |x| x.is_a? Hash }
      headers.each do |k, v|
        @request[k] = v
      end
    end

    private

    def create_uri(url)
      raise_error(url, 'not of type String') { |x| x.is_a? String }
      @uri = URI.parse(url)
    end

    def ssl_options
      { use_ssl: @uri.scheme.eql?('https') }
    end

    def raise_error(object, message)
      raise Errors::HTTPError.new(:parameter, 500, message) unless yield object
    end
  end
end
