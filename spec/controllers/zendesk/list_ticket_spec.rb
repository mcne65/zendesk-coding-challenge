# frozen_string_literal: true

require 'rails_helper'
require_relative '../../lib/zendesk/zendesk_helper.rb'

RSpec.describe ZendeskController, type: :controller do
  describe 'GET list_tickets' do
    before { @valid_params = { 'page' => '1' } }

    context 'parameters' do
      context 'valid' do
        before do
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=#{@valid_params['page']}" \
            "&per_page=#{ENV['ZENDESK_TICKETS_PER_PAGE']}")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: ticket_list_data.to_json)
          send_request
        end

        it 'should not return alert' do
          expect(assigns['alert']).to be_nil
        end

        it 'should not return error' do
          expect(assigns['error']).to be_nil
        end

        it 'should not return message' do
          expect(assigns['message']).to be_nil
        end

        it 'should not return code' do
          expect(assigns['code']).to be_nil
        end

        context 'response' do
          it 'should not be nil' do
            expect(assigns['response']).to_not be_nil
          end

          it 'should include zendesk http code' do
            expect(assigns['response'].code).to match(200)
          end

          it 'should include zendesk ticket list information' do
            expect(assigns['response'].body).to match(ticket_list_data)
          end
        end
      end

      context 'invalid' do
        before do
          params = { 'page' => '2000' }
          stub_request(:get, "#{ENV['ZENDESK_URL']}/api/v2/tickets.json?page=#{params['page']}" \
            "&per_page=#{ENV['ZENDESK_TICKETS_PER_PAGE']}")
            .with(basic_auth: [ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']])
            .to_return(status: 200, body: empty_ticket_list_data.to_json)
          send_request(params)
        end

        it 'should return alert' do
          expect(assigns['alert']).to be_truthy
        end

        it 'should return error' do
          expect(assigns['error']).to match('Zendesk')
        end

        it 'should return message' do
          expect(assigns['message']).to_not be_nil
        end

        it 'should return code' do
          expect(assigns['code']).to match(204)
        end
      end
    end
  end

  def send_request(params = @valid_params)
    get :list_tickets, params: params
  end
end
