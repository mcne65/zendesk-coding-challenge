# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  describe 'GET #home' do
    before { send_request }
  end

  def send_request
    get :home
  end
end
