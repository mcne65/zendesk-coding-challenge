# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/http/get.rb'

URL = 'https://www.google.com'

RSpec.describe HTTP::Get do
  context 'initialization' do
    context 'parameters' do
      it 'should only accept one variable' do
        expect { HTTP::Get.new }.to raise_error(ArgumentError)
        expect { HTTP::Get.new('a', 'b') }.to raise_error(ArgumentError)
        expect { HTTP::Get.new(URL) }.to_not raise_error
      end

      it 'should raise an error if string fails uri regex' do
        expect { HTTP::Get.new('abc') }.to raise_error(ArgumentError)
      end

      context 'invalid' do
        it 'should raise an error if not of type String' do
          expect { HTTP::Get.new(1) }.to raise_error(Errors::HTTPError)
        end

        context 'error' do
          it 'should have 500 status' do
            HTTP::Get.new(1)
          rescue Errors::HTTPError => e
            expect(e.status).to match(500)
          end

          it 'should have :parameter error' do
            HTTP::Get.new(1)
          rescue Errors::HTTPError => e
            expect(e.error).to match(:parameter)
          end

          it 'should have a message' do
            HTTP::Get.new(1)
          rescue Errors::HTTPError => e
            expect(e.message).to match('not of type String')
          end
        end
      end
    end

    describe 'instance variables' do
      before { @http_object = HTTP::Get.new(URL) }

      describe 'URI object' do
        before { @uri = @http_object.uri }

        it 'should exist' do
          expect(@uri).to be_an(URI)
        end

        it 'should point to the given url' do
          expect(@uri.to_s).to match(URL)
        end

        it 'should be uneditable' do
          expect { @http_object.uri = 'uri' }.to raise_error(NoMethodError)
        end
      end

      describe 'Net::HTTP::Get object' do
        before { @request = @http_object.request }

        it 'should exist' do
          expect(@request).to be_a(Net::HTTP::Get)
        end

        it 'should be uneditable' do
          expect { @http_object.request = 'uri' }.to raise_error(NoMethodError)
        end
      end
    end
  end

  context 'basic authentication' do
    before { @http_object = HTTP::Get.new(URL) }

    describe 'parameters' do
      it 'should only accept two variables' do
        expect { @http_object.basic_auth('a') }.to raise_error(ArgumentError)
        expect { @http_object.basic_auth('a', 'b', 'c') }.to raise_error(ArgumentError)
        expect { @http_object.basic_auth(ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']) }.to_not raise_error
      end

      context 'invalid' do
        it 'should raise error if not of type String' do
          expect { @http_object.basic_auth(1, '123') }.to raise_error(Errors::HTTPError)
          expect { @http_object.basic_auth('123', 1) }.to raise_error(Errors::HTTPError)
        end

        context 'error' do
          it 'should have 500 status' do
            @http_object.basic_auth(1, '123')
          rescue Errors::HTTPError => e
            expect(e.status).to match(500)
          end

          it 'should have :parameter error' do
            @http_object.basic_auth(1, '123')
          rescue Errors::HTTPError => e
            expect(e.error).to match(:parameter)
          end

          it 'should have a message' do
            @http_object.basic_auth(1, '123')
          rescue Errors::HTTPError => e
            expect(e.message).to match('not of type String')
          end
        end
      end
    end

    describe 'result' do
      before { @http_object.basic_auth(ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD']) }

      it 'should create an authorization header that starts with "Basic"' do
        expect(@http_object.request['authorization']).to match(/^Basic /)
      end
    end
  end

  context 'request headers' do
    before do
      @http_object = HTTP::Get.new(URL)
      @http_object.basic_auth(ENV['ZENDESK_USERNAME'], ENV['ZENDESK_PASSWORD'])
    end

    describe 'parameters' do
      before { @parameter = { 'content-type': 'application/json' } }

      it 'should only accept one variable' do
        expect { @http_object.request_headers }.to raise_error(ArgumentError)
        expect { @http_object.request_headers(@parameter, 'b') }.to raise_error(ArgumentError)
        expect { @http_object.request_headers(@parameter) }.to_not raise_error
      end

      context 'invalid' do
        it 'should raise an error if not of type Hash' do
          expect { @http_object.request_headers('123') }.to raise_error(Errors::HTTPError)
        end

        context 'error' do
          it 'should have 500 status' do
            @http_object.request_headers('123')
          rescue Errors::HTTPError => e
            expect(e.status).to match(500)
          end

          it 'should have :parameter error' do
            @http_object.request_headers('123')
          rescue Errors::HTTPError => e
            expect(e.error).to match(:parameter)
          end

          it 'should have a message' do
            @http_object.request_headers('123')
          rescue Errors::HTTPError => e
            expect(e.message).to match('not of type Hash')
          end
        end
      end
    end

    describe 'result' do
      before do
        @http_object.request_headers('Content-Type': 'application/json')
        @request = @http_object.request
      end

      it 'should add request headers' do
        expect(@request['Content-Type']).to match('application/json')
      end

      it 'should alter request headers' do
        expect(@request['authorization']).to match(/^Basic /)
        @http_object.request_headers('authorization': 'Bearer')
        @request = @http_object.request
        expect(@request['authorization']).to match('Bearer')
      end
    end
  end

  context 'send request' do
    before do
      @http_object = HTTP::Get.new(URL)
      stub_request(:get, URL)
    end

    it 'should return a response' do
      response = @http_object.send
      expect(response.code).to match('200')
    end
  end
end
