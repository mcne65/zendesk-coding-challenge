# frozen_string_literal: true

def authentication_failed_data
  {
    'error' => "Couldn't authenticate you"
  }
end

def subdomain_invalid_data
  {
    'error' => {
      'title' => 'No help desk at bluepandaa.zendesk.com',
      'message' => 'There is no help desk configured at this address. ' \
                   'This means that the address is available and that you can claim it at ' \
                   'http://www.zendesk.com/signup'
    }
  }
end

# rubocop:disable Metrics/MethodLength
def ticket_list_data
  {
    'tickets' => [
      {
        'url' => 'https://subdomain.zendesk.com/api/v2/tickets.json?page=1&per_page=1',
        'id' => 1,
        'external_id' => nil,
        'via' => {
          'channel' => 'sample_ticket',
          'source' => {
            'from' => {},
            'to' => {},
            'rel' => nil
          }
        },
        'created_at' => '2019-06-03T12:04:28Z',
        'updated_at' => '2019-06-03T12:04:30Z',
        'type' => 'incident',
        'subject' => 'Sample ticket: Meet the ticket',
        'raw_subject' => 'Sample ticket: Meet the ticket',
        'description' => "Hi Alvin,\n\nThis is your first ticket.",
        'priority' => 'normal',
        'status' => 'open',
        'recipient' => nil,
        'requester_id' => 381_288_910_974,
        'submitter_id' => 381_271_177_574,
        'assignee_id' => 381_271_177_574,
        'organization_id' => nil,
        'group_id' => 360_004_509_934,
        'collaborator_ids' => [],
        'follower_ids' => [],
        'email_cc_ids' => [],
        'forum_topic_id' => nil,
        'problem_id' => nil,
        'has_incidents' => false,
        'is_public' => true,
        'due_at' => nil,
        'tags' => %w[sample support zendesk],
        'custom_fields' => [],
        'satisfaction_rating' => nil,
        'sharing_agreement_ids' => [],
        'fields' => [],
        'followup_ids' => [],
        'brand_id' => 360_002_612_574,
        'allow_channelback' => false,
        'allow_attachments' => true
      }
    ],
    'next_page' => 'https://subdomain.zendesk.com/api/v2/tickets.json?page=2&per_page=1',
    'previous_page' => nil,
    'count' => 101
  }
end
# rubocop:enable Metrics/MethodLength

def empty_ticket_list_data
  {
    'tickets' => [],
    'next_page' => nil,
    'previous_page' => nil,
    'count' => 0
  }
end

# rubocop:disable Metrics/MethodLength
def ticket_data
  {
    'ticket' => {
      'url' => 'https://subdomain.zendesk.com/api/v2/tickets/1.json',
      'id' => 1,
      'external_id' => nil,
      'via' => {
        'channel' => 'sample_ticket',
        'source' => {
          'from' => {},
          'to' => {},
          'rel' => nil
        }
      },
      'created_at' => '2019-06-03T12:04:28Z',
      'updated_at' => '2019-06-03T12:04:30Z',
      'type' => 'incident',
      'subject' => 'Sample ticket: Meet the ticket',
      'raw_subject' => 'Sample ticket: Meet the ticket',
      'description' => "Hi Alvin,\n\nThis is your first ticket.",
      'priority' => 'normal',
      'status' => 'open',
      'recipient' => nil,
      'requester_id' => 381_288_910_974,
      'submitter_id' => 381_271_177_574,
      'assignee_id' => 381_271_177_574,
      'organization_id' => nil,
      'group_id' => 360_004_509_934,
      'collaborator_ids' => [],
      'follower_ids' => [],
      'email_cc_ids' => [],
      'forum_topic_id' => nil,
      'problem_id' => nil,
      'has_incidents' => false,
      'is_public' => true,
      'due_at' => nil,
      'tags' => %w[sample support zendesk],
      'custom_fields' => [],
      'satisfaction_rating' => nil,
      'sharing_agreement_ids' => [],
      'fields' => [],
      'followup_ids' => [],
      'brand_id' => 360_002_612_574,
      'allow_channelback' => false,
      'allow_attachments' => true
    }
  }
end
# rubocop:enable Metrics/MethodLength

def empty_ticket_data
  {
    'error' => 'RecordNotFound',
    'description' => 'Not found'
  }
end
