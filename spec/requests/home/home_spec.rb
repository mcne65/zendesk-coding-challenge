# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomeController, type: :request do
  describe 'GET #home' do
    before { send_request }
    context 'status code' do
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end
    end

    context 'view' do
      it 'should render home template' do
        expect(response).to render_template('home')
      end
    end
  end

  def send_request
    get root_path
  end
end
